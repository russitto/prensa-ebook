const nodemailer = require('nodemailer')
const { exec } = require('child_process')

const _l = console.log.bind(console) // eslint-disable-line no-console,no-unused-vars
const _e = console.error.bind(console) // eslint-disable-line no-console,no-unused-vars
const dat = new Date().toISOString().replace(/(:|\.)/g, '_')

const isEmail = process.env['PEEMAIL'] && process.env['PEEPASS']

const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: process.env['PEEMAIL'],
    pass: process.env['PEEPASS']
  }
})

const pdf = 'book_' + dat + '.pdf'
const mobi = 'book_' + dat + '.mobi'

_l('** generating PDF & mobi')
Promise.all([
  execProm('npx gitbook pdf assets ' + pdf),
  execProm('npx gitbook mobi assets ' + mobi)
]).then(() => {
  if (!isEmail) {
    _l('no config email')
    return
  }
  _l('** sending pdf email')
  let mailOptions = {
    from: 'mrussitto@qubit.tv',
    to: 'russitto_paperwhite_2015@kindle.com',
    subject: 'Ultima Prensa pdf ' + dat,
    text: 'Sale con fritas! pdf ' + dat,
    attachments: [{path: pdf}]
  }
  transporter.sendMail(mailOptions)

  _l('** sending mobi email')
  mailOptions = {
    from: 'mrussitto@qubit.tv',
    to: 'russitto_paperwhite_2015@kindle.com',
    subject: 'Ultima Prensa mobi ' + dat,
    text: 'Sale con fritas! mobi ' + dat,
    attachments: [{path: mobi}]
  }
  transporter.sendMail(mailOptions)
  return
}).catch(err => {
  _e(err)
})

function execProm(cmd) {
  return new Promise((res, rej) => {
    exec(cmd, (err, stdout, stderr) => {
      if (err) {
        return rej(stderr)
      }
      res(stdout)
    })
  })
}
