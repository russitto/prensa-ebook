const fs = require('fs')
const miniget = require('miniget')
const DOMParser = require('dom-parser')
const TurndownService = require('turndown')

const _l = console.log.bind(console) // eslint-disable-line no-console,no-unused-vars
const _e = console.error.bind(console) // eslint-disable-line no-console,no-unused-vars
const _dir = __dirname  + '/assets/'

const BASE = 'https://prensaobrera.com'
const domParser = new DOMParser()
const tdown = new TurndownService()

get(BASE + '/prensaObrera/ultima', true
).then(html => {
  const left = html.indexOf('<h1 class="titulo-seccion">')
  const right = html.indexOf('<script', left)
  const fancy = html.match(/<a href="([a-zA-Z0-9\/\-_\.]+)" class="fancybox">/)
  const title = html.match(/<title>(.*)<\/title>/)
  if (left >= 0 && right >= 0) {
    html = html.substring(left, right)
  }

  const document = domParser.parseFromString(html)

  let mainTitle = '', mainImg = '', urls = []

  if (fancy.length > 1) {
    mainImg = BASE + fancy[1]
  }
  if (title.length > 1) {
    mainTitle = title[1]
  }

  document.getElementsByTagName('a').forEach(anchor => {
    let href = anchor.getAttribute('href')
    if (!href) return
    let text = anchor.textContent
    let slug = href.replace(/.*\//, '')
    href = BASE + href
    urls.push({slug, href, text})
  })
  return writeFiles(mainTitle, mainImg, urls)
}).catch(err => {
  _e(err)
})

function get(url) {
  _l('** getting: ' + url)
  return new Promise((resolve, reject) => {
    miniget(url, (err, resp, body) => {
      if (err) {
        return reject(err)
      }
      resolve(body)
    })
  })
}

function writeFiles(mainTitle, mainImg, urls) {
  return Promise.all([
    downloadImage(mainImg, 'cover.jpg'),
    writeSummary(mainTitle, urls),
    writePublications(urls)
  ])
}

function writeSummary(mainTitle, urls) {
  const data = urls.map(url => `* [${url.text}](${url.slug}.md)`)
  const txt = `# ${mainTitle}

![${mainTitle}](cover.jpg)
`
  return Promise.all([
    writeFile('SUMMARY.md', '* [Introducción](README.md)\n' + data.join('\n')),
    writeFile('README.md', txt)
  ])
}

function writePublications(urls) {
  return Promise.all(urls.map(url => writePublication(url)))
}

function writePublication(url) {
  let mdown = ''
  return get(url.href
  ).then(bightml => {
    let {html, img} = extractHtml(bightml, url.slug)
    mdown = tdown.turndown(html)
    if (img) {
      return downloadImage(img, url.slug + '.jpg')
    }
    return true
  }).then(() => {
    return writeFile(url.slug + '.md', mdown)
  }).catch(e => {
    _e(e)
  })
}

function extractHtml(html, slug) {
  const dom = domParser.parseFromString(html)
  const content = dom.getElementById('content')
  const authors = content.getElementsByClassName('autor')
  const subcontent = content.getElementsByClassName('contenido')
  const h1s = content.getElementsByTagName('h1')
  let title = ''
  let by = ''
  let img = ''
  if (!subcontent.length) {
    return ''
  }
  if (h1s.length) {
    title = h1s[0].innerHTML
  }
  if (authors.length) {
    by = authors[0].innerHTML
  }
  const fancy = subcontent[0].getElementsByClassName('fancybox')
  if (fancy.length) {
    const fancyImg = fancy[0].getElementsByTagName('img')
    if (fancyImg.length) {
      img = fancyImg[0].getAttribute('src')
    }
  }
  let out = '<h1>' + title + '</h1>' + '<h4>' + by + '</h4>'
  if (img != '') {
    if (img.indexOf('http') != 0) {
      img = BASE + img
    }
    out += '<img src="' + slug + '.jpg">'
  }
  const subHtml = subcontent[0].innerHTML.replace(/<img src=".*"\/>/g, '')
  out += subHtml

  return {html: out, img}
}

function writeFile(file, txt) {
  _l('** writting: ' + file)
  return new Promise((resolve, reject) => {
    fs.writeFile(_dir  + file, txt, 'utf8', err => {
      if (err) return reject(err)
      resolve()
    })
  })
}

function downloadImage(img, name) {
  _l('** downloading img: ' + img)
  const file = fs.createWriteStream(_dir + name)
  return new Promise((resolve, reject) => {
    let m
    try {
      m = miniget(img).pipe(file)
    } catch(e) {
      return reject(e) 
    }
    resolve(m)
  })
}
